﻿create database Ginasio
go
use ginasio
go

CREATE TABLE Socios(
	IDSocio INTEGER NOT NULL IDENTITY (1,1),
	email VARCHAR(100) NOT NULL UNIQUE,
	telefone VARCHAR(20) NOT NULL,
	fotografia VARCHAR(40) NOT NULL, 
	sexo BIT NOT NULL DEFAULT 0,--BIT é  o boleano, false 0 - Masculino, true 1 - Feminino
	altura FLOAT NOT NULL DEFAULT 0,
	nome_utilizador VARCHAR(50) NOT NULL UNIQUE,
	peso_inicial FLOAT NOT NULL DEFAULT 0,
	_password VARCHAR(20) NOT NULL,
	estado INTEGER NOT NULL,--0 - nao esta suspenso,  1 - suspenso , 2 -  Tem pt , 3 - Não tem PT
	PRIMARY KEY(IDSocio),
	CHECK (peso_inicial>0)
);

CREATE TABLE Professores(
	IDProfessor INTEGER NOT NULL IDENTITY(1,1),
	nome VARCHAR(50) NOT NULL,
	email VARCHAR(100) NOT NULL UNIQUE,
	telefone INTEGER NOT NULL,
	fotografia VARCHAR(40) NOT NULL,
	sexo BIT NOT NULL DEFAULT 0,--BIT é  o boleano, false 0 - Masculino, true 1 - Feminino
	especialidade VARCHAR(50) NOT NULL,
	estado  INTEGER NOT NULL DEFAULT 0,  -- 0 -nao suspenso,  1 - suspenso, 2 -  É PT , 3 - Não é PT
	_password VARCHAR(20) NOT NULL ,
	PRIMARY KEY(IDProfessor)
	
);

CREATE TABLE Administrador(
	IDAdministrador INTEGER NOT NULL IDENTITY(1,1),
	nome VARCHAR(50) NOT NULL,
	email VARCHAR(100) NOT NULL,
	password VARCHAR(30) NOT NULL,
	PRIMARY KEY(IDAdministrador)
);

CREATE TABLE Personal_trainer(
	IDProfessor INTEGER NOT NULL,
	IDSocio Integer not null,
	Data_Pedido date default getdate() not null,
	data_Inicio DATE null,
	data_fim DATE NULL,
	PRIMARY KEY(IDProfessor,IDSocio,data_Pedido),
	FOREIGN KEY(IDSocio) REFERENCES Socios(IDSocio),
	FOREIGN KEY(IDProfessor) REFERENCES Professores(IDProfessor)
);

CREATE TABLE Aulas_Grupo(
	IDAulas_Grupo INTEGER NOT NULL IDENTITY (1,1),
	nome VARCHAR(50) NOT NULL,
	fotografia VARCHAR(30) NOT NULL ,
	texto_descritivo VARCHAR(1000) NOT NULL,
	estado BIT NOT NULL DEFAULT 0,--BIT é  o boleano, false 0 -ativa, true 1 - desativada
	PRIMARY KEY(IDAulas_Grupo)
);
CREATE TABLE Mapa_Aulas_Grupo(
	IDMapa_Aulas INTEGER NOT NULL IDENTITY(1,1),
	IDAulas_Grupo integer not null,
	IDProfessor INTEGER NOT NULL,
	lotacao INTEGER NOT NULL DEFAULT 0,
	--periodicidade VARCHAR(20) NOT NULL, 
	Dia_da_Semana Varchar(30) NOT NULL,
	hora TIME NOT NULL,
	PRIMARY KEY(IDMapa_Aulas),
	FOREIGN KEY(IDProfessor) REFERENCES Professores(IDProfessor),
	foreign key(idAulas_grupo) references Aulas_Grupo(IDAulas_Grupo)
);
CREATE TABLE Participa(
	_data DATE DEFAULT GETDATE() not null,
	IDSocio INTEGER NOT NULL ,
	IDMapa_Aulas INTEGER NOT NULL ,
	PRIMARY KEY (IDSocio,_data,IDMapa_Aulas), 
	FOREIGN KEY(IDSocio) REFERENCES Socios(IDSocio),
	FOREIGN KEY(IDMapa_Aulas) REFERENCES Mapa_Aulas_Grupo(IDMapa_Aulas)
);
CREATE TABLE Exercicios_Ginasio(
	IDExercicios_Ginasio INTEGER NOT NULL IDENTITY(1,1),
	nome VARCHAR(30) NOT NULL,
	texto_descritivo VARCHAR(1000) NOT NULL,
	foto  VARCHAR(50) NOT NULL,
	video VARCHAR(50) NOT NULL, --VAMSO GUARDAR APENAS O NOME 
	PRIMARY KEY(IDExercicios_Ginasio)
);
CREATE TABLE Planos_Exercicios(
	IDPlanos_Exercicios INTEGER NOT NULL IDENTITY(1,1),
	IDSocio INTEGER NOT NULL ,
	IDProfessor integer not null,
	--duracao TIME NOT NULL,
	data_inicio DATE DEFAULT GETDATE() not null,
	data_fim DATE   NULL,
	PRIMARY KEY(IDPlanos_Exercicios),
	FOREIGN KEY(IDSocio) REFERENCES Socios(IDSocio),
	FOREIGN KEY(IDProfessor) REFERENCES Professores(IDProfessor)

);

CREATE TABLE Inclui(
	duracao TIME  NULL,
	repeticoes INTEGER  NULL,
	IdPlanodeexercicios Integer not null,
	IDExercicios_Ginasio INTEGER NOT NULL ,
	PRIMARY KEY(IDExercicios_Ginasio,Idplanodeexercicios),

	FOREIGN KEY(IdPlanodeexercicios) REFERENCES Planos_Exercicios(IDPlanos_Exercicios),
	FOREIGN KEY(IDExercicios_Ginasio) REFERENCES Exercicios_Ginasio(IDExercicios_Ginasio)
);


CREATE TABLE Gerir(
	data_suspensao DATE DEFAULT GETDATE() NOT NULL,
	motivo_suspensao varchar(100) not null,
	IDSocio int not null,
	IDAdministrador INTEGER NOT NULL,
	PRIMARY KEY(IDSocio,IDAdministrador),
	FOREIGN KEY(IDSocio) REFERENCES Socios(IDSocio),
	FOREIGN KEY(IDAdministrador) REFERENCES Administrador(IDAdministrador)
);

CREATE TABLE Peso(
	Data_Pesagem DATE DEFAULT GETDATE() not null,
	IDSocio INTEGER NOT NULL  ,
	IDProfessor INTEGER NOT NULL  ,
	valor Float NOT NULL,
	PRIMARY KEY(IDSocio,IDProfessor,Data_Pesagem),
	FOREIGN KEY(IDSocio) REFERENCES Socios(IDSocio),
	FOREIGN KEY(IDProfessor) REFERENCES Professores(IDProfessor)
);



CREATE TABLE Mensagem(
	Data_Envio DATE DEFAULT GETDATE(),
	texto VARCHAR(1000) NOT NULL,
	Data_leitura Date  NULL, -- O NAO LIDA , 1-LIDA
	direcao Bit not null, --0 professor para aluno e 1 aluno professor
	IDSocio INTEGER NOT NULL ,
	IDProfessor INTEGER NOT NULL  ,
	PRIMARY KEY (Idsocio,IdProfessor,Data_Envio), 
	FOREIGN KEY(IDSocio) REFERENCES Socios(IDSocio),
	FOREIGN KEY(IDProfessor) REFERENCES Professores(IDProfessor)
);
