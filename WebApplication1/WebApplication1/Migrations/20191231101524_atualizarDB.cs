﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class atualizarDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Aulas_Grupo",
                columns: table => new
                {
                    IDAulas_Grupo = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nome = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    fotografia = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    texto_descritivo = table.Column<string>(unicode: false, maxLength: 1000, nullable: false),
                    estado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Aulas_Gr__98605FD23F3BA93F", x => x.IDAulas_Grupo);
                });

            migrationBuilder.CreateTable(
                name: "Exercicios_Ginasio",
                columns: table => new
                {
                    IDExercicios_Ginasio = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nome = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    texto_descritivo = table.Column<string>(unicode: false, maxLength: 1000, nullable: false),
                    foto = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    video = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Exercici__8CFA31663F63B859", x => x.IDExercicios_Ginasio);
                });

            migrationBuilder.CreateTable(
                name: "Professores",
                columns: table => new
                {
                    IDProfessor = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nome = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    telefone = table.Column<int>(nullable: false),
                    fotografia = table.Column<string>(unicode: false, maxLength: 40, nullable: false),
                    sexo = table.Column<bool>(nullable: false),
                    especialidade = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    estado = table.Column<int>(nullable: false),
                    _password = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Professo__38B0B8AD9D8ECE87", x => x.IDProfessor);
                });

            migrationBuilder.CreateTable(
                name: "Socios",
                columns: table => new
                {
                    IDSocio = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    email = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    telefone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    fotografia = table.Column<string>(unicode: false, maxLength: 40, nullable: false),
                    sexo = table.Column<bool>(nullable: false),
                    altura = table.Column<double>(nullable: false),
                    nome_utilizador = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    peso_inicial = table.Column<double>(nullable: false),
                    _password = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    estado = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Socios__05E84F1CC4F44E18", x => x.IDSocio);
                });

            migrationBuilder.CreateTable(
                name: "utilizadores",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 150, nullable: false),
                    Email = table.Column<string>(maxLength: 200, nullable: false),
                    Username = table.Column<string>(maxLength: 100, nullable: false),
                    Password = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_utilizadores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Mapa_Aulas_Grupo",
                columns: table => new
                {
                    IDMapa_Aulas = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IDAulas_Grupo = table.Column<int>(nullable: false),
                    IDProfessor = table.Column<int>(nullable: false),
                    lotacao = table.Column<int>(nullable: false),
                    Dia_da_Semana = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    hora = table.Column<TimeSpan>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Mapa_Aul__4235D4C6653F3DE1", x => x.IDMapa_Aulas);
                    table.ForeignKey(
                        name: "FK__Mapa_Aula__IDAul__3B75D760",
                        column: x => x.IDAulas_Grupo,
                        principalTable: "Aulas_Grupo",
                        principalColumn: "IDAulas_Grupo",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Mapa_Aula__IDPro__3A81B327",
                        column: x => x.IDProfessor,
                        principalTable: "Professores",
                        principalColumn: "IDProfessor",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Mensagem",
                columns: table => new
                {
                    Data_Envio = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getdate())"),
                    IDSocio = table.Column<int>(nullable: false),
                    IDProfessor = table.Column<int>(nullable: false),
                    texto = table.Column<string>(unicode: false, maxLength: 1000, nullable: false),
                    Data_leitura = table.Column<DateTime>(type: "date", nullable: true),
                    direcao = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Mensagem__76105D87751398BF", x => new { x.IDSocio, x.IDProfessor, x.Data_Envio });
                    table.ForeignKey(
                        name: "FK__Mensagem__IDProf__59063A47",
                        column: x => x.IDProfessor,
                        principalTable: "Professores",
                        principalColumn: "IDProfessor",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Mensagem__IDSoci__5812160E",
                        column: x => x.IDSocio,
                        principalTable: "Socios",
                        principalColumn: "IDSocio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Personal_trainer",
                columns: table => new
                {
                    IDProfessor = table.Column<int>(nullable: false),
                    IDSocio = table.Column<int>(nullable: false),
                    Data_Pedido = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getdate())"),
                    data_Inicio = table.Column<DateTime>(type: "date", nullable: true),
                    data_fim = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Personal__BE10AAD5C2D11811", x => new { x.IDProfessor, x.IDSocio, x.Data_Pedido });
                    table.ForeignKey(
                        name: "FK__Personal___IDPro__33D4B598",
                        column: x => x.IDProfessor,
                        principalTable: "Professores",
                        principalColumn: "IDProfessor",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Personal___IDSoc__32E0915F",
                        column: x => x.IDSocio,
                        principalTable: "Socios",
                        principalColumn: "IDSocio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Peso",
                columns: table => new
                {
                    Data_Pesagem = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getdate())"),
                    IDSocio = table.Column<int>(nullable: false),
                    IDProfessor = table.Column<int>(nullable: false),
                    valor = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Peso__491D34FE7F32B0CB", x => new { x.IDSocio, x.IDProfessor, x.Data_Pesagem });
                    table.ForeignKey(
                        name: "FK__Peso__IDProfesso__5441852A",
                        column: x => x.IDProfessor,
                        principalTable: "Professores",
                        principalColumn: "IDProfessor",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Peso__IDSocio__534D60F1",
                        column: x => x.IDSocio,
                        principalTable: "Socios",
                        principalColumn: "IDSocio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Planos_Exercicios",
                columns: table => new
                {
                    IDPlanos_Exercicios = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IDSocio = table.Column<int>(nullable: false),
                    IDProfessor = table.Column<int>(nullable: false),
                    data_inicio = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getdate())"),
                    data_fim = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Planos_E__C0C8EF5D1FD3319A", x => x.IDPlanos_Exercicios);
                    table.ForeignKey(
                        name: "FK__Planos_Ex__IDPro__46E78A0C",
                        column: x => x.IDProfessor,
                        principalTable: "Professores",
                        principalColumn: "IDProfessor",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Planos_Ex__IDSoc__45F365D3",
                        column: x => x.IDSocio,
                        principalTable: "Socios",
                        principalColumn: "IDSocio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Administrador",
                columns: table => new
                {
                    IDAdministrador = table.Column<int>(nullable: false),
                    nome = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    password = table.Column<string>(unicode: false, maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Administ__B1D14F091A3782D6", x => x.IDAdministrador);
                    table.ForeignKey(
                        name: "FK_Administrador_utilizadores_IDAdministrador",
                        column: x => x.IDAdministrador,
                        principalTable: "utilizadores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "seguranca",
                columns: table => new
                {
                    UtilizadorId = table.Column<int>(nullable: false),
                    CarteiraProfissional = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seguranca", x => x.UtilizadorId);
                    table.ForeignKey(
                        name: "FK_seguranca_utilizadores_UtilizadorId",
                        column: x => x.UtilizadorId,
                        principalTable: "utilizadores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Participa",
                columns: table => new
                {
                    _data = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getdate())"),
                    IDSocio = table.Column<int>(nullable: false),
                    IDMapa_Aulas = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Particip__4D0D16292AFF2034", x => new { x.IDSocio, x._data, x.IDMapa_Aulas });
                    table.ForeignKey(
                        name: "FK__Participa__IDMap__403A8C7D",
                        column: x => x.IDMapa_Aulas,
                        principalTable: "Mapa_Aulas_Grupo",
                        principalColumn: "IDMapa_Aulas",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Participa__IDSoc__3F466844",
                        column: x => x.IDSocio,
                        principalTable: "Socios",
                        principalColumn: "IDSocio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Inclui",
                columns: table => new
                {
                    IdPlanodeexercicios = table.Column<int>(nullable: false),
                    IDExercicios_Ginasio = table.Column<int>(nullable: false),
                    duracao = table.Column<TimeSpan>(nullable: true),
                    repeticoes = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Inclui__5B1E074513A68E66", x => new { x.IDExercicios_Ginasio, x.IdPlanodeexercicios });
                    table.ForeignKey(
                        name: "FK__Inclui__IdPlanod__49C3F6B7",
                        column: x => x.IdPlanodeexercicios,
                        principalTable: "Planos_Exercicios",
                        principalColumn: "IDPlanos_Exercicios",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Inclui__IDExerci__4AB81AF0",
                        column: x => x.IDExercicios_Ginasio,
                        principalTable: "Exercicios_Ginasio",
                        principalColumn: "IDExercicios_Ginasio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Gerir",
                columns: table => new
                {
                    IDSocio = table.Column<int>(nullable: false),
                    IDAdministrador = table.Column<int>(nullable: false),
                    data_suspensao = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getdate())"),
                    motivo_suspensao = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Gerir__8EF55BEC57939965", x => new { x.IDSocio, x.IDAdministrador });
                    table.ForeignKey(
                        name: "FK__Gerir__IDAdminis__4F7CD00D",
                        column: x => x.IDAdministrador,
                        principalTable: "Administrador",
                        principalColumn: "IDAdministrador",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Gerir__IDSocio__4E88ABD4",
                        column: x => x.IDSocio,
                        principalTable: "Socios",
                        principalColumn: "IDSocio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Gerir_IDAdministrador",
                table: "Gerir",
                column: "IDAdministrador");

            migrationBuilder.CreateIndex(
                name: "IX_Inclui_IdPlanodeexercicios",
                table: "Inclui",
                column: "IdPlanodeexercicios");

            migrationBuilder.CreateIndex(
                name: "IX_Mapa_Aulas_Grupo_IDAulas_Grupo",
                table: "Mapa_Aulas_Grupo",
                column: "IDAulas_Grupo");

            migrationBuilder.CreateIndex(
                name: "IX_Mapa_Aulas_Grupo_IDProfessor",
                table: "Mapa_Aulas_Grupo",
                column: "IDProfessor");

            migrationBuilder.CreateIndex(
                name: "IX_Mensagem_IDProfessor",
                table: "Mensagem",
                column: "IDProfessor");

            migrationBuilder.CreateIndex(
                name: "IX_Participa_IDMapa_Aulas",
                table: "Participa",
                column: "IDMapa_Aulas");

            migrationBuilder.CreateIndex(
                name: "IX_Personal_trainer_IDSocio",
                table: "Personal_trainer",
                column: "IDSocio");

            migrationBuilder.CreateIndex(
                name: "IX_Peso_IDProfessor",
                table: "Peso",
                column: "IDProfessor");

            migrationBuilder.CreateIndex(
                name: "IX_Planos_Exercicios_IDProfessor",
                table: "Planos_Exercicios",
                column: "IDProfessor");

            migrationBuilder.CreateIndex(
                name: "IX_Planos_Exercicios_IDSocio",
                table: "Planos_Exercicios",
                column: "IDSocio");

            migrationBuilder.CreateIndex(
                name: "UQ__Professo__AB6E616417C6F033",
                table: "Professores",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UQ__Socios__AB6E61645D151F00",
                table: "Socios",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UQ__Socios__24D6AA7FBE361D5F",
                table: "Socios",
                column: "nome_utilizador",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Gerir");

            migrationBuilder.DropTable(
                name: "Inclui");

            migrationBuilder.DropTable(
                name: "Mensagem");

            migrationBuilder.DropTable(
                name: "Participa");

            migrationBuilder.DropTable(
                name: "Personal_trainer");

            migrationBuilder.DropTable(
                name: "Peso");

            migrationBuilder.DropTable(
                name: "seguranca");

            migrationBuilder.DropTable(
                name: "Administrador");

            migrationBuilder.DropTable(
                name: "Planos_Exercicios");

            migrationBuilder.DropTable(
                name: "Exercicios_Ginasio");

            migrationBuilder.DropTable(
                name: "Mapa_Aulas_Grupo");

            migrationBuilder.DropTable(
                name: "utilizadores");

            migrationBuilder.DropTable(
                name: "Socios");

            migrationBuilder.DropTable(
                name: "Aulas_Grupo");

            migrationBuilder.DropTable(
                name: "Professores");
        }
    }
}
