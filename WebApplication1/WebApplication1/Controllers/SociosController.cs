﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using WebApplication1.filters;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{

    public class SociosController : Controller
    {
        private readonly GinasioContext _context;

        private readonly IHostEnvironment _hostEnviroment;

        public SociosController(GinasioContext context, IHostEnvironment env)
        {
            _context = context;
            _hostEnviroment = env;
        }

        // GET: Socios
        public async Task<IActionResult> Index()
        {
            ViewBag.userId = HttpContext.Session.GetInt32("UserId");
            ViewBag.Nome = HttpContext.Session.GetString("Nome");
            return View(await _context.Socios.ToListAsync());
        }

        public async Task<IActionResult> ListarSociosAdmin()
        {
            ViewBag.userId = HttpContext.Session.GetInt32("UserId");
            ViewBag.Nome = HttpContext.Session.GetString("Nome");
            return View(await _context.Socios.ToListAsync());
        }


        // GET: Socios/Details/5
        public async Task<IActionResult> Details(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var socios = await _context.Socios
                .FirstOrDefaultAsync(m => m.Idsocio == id);
            if (socios == null)
            {
                return NotFound();
            }

            return View(socios);
           
            


            //if((professores.Idprofessor== personal.Idprofessor) & (socios.Idsocio == personal.Idsocio))
            //{
            //   //return
            //}
            //else
            //{
            //    //return ("Details");
            //}
        }

        public async Task<IActionResult> EditarEstadoSocio(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var socios = await _context.Socios.FindAsync(id);
            if (socios == null)
            {
                return NotFound();
            }
            return View(socios);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> EditarEstadoSocio(int id, [Bind("Idsocio,Email,Telefone,Fotografia,Sexo,Altura,NomeUtilizador,PesoInicial,Password,Estado")] Socios socios, IFormFile fotografia)
        {
            string caminho = Path.Combine(_hostEnviroment.ContentRootPath, "wwwroot\\Fotos");

            string nome_ficheiro = Path.GetFileName(fotografia.FileName);
            string caminho_completo = Path.Combine(caminho, nome_ficheiro);

            FileStream fs = new FileStream(caminho_completo, FileMode.Create);
            fotografia.CopyTo(fs);
            socios.Fotografia = nome_ficheiro;


            fs.Close();
            if (id != socios.Idsocio)
            {
                return NotFound();
            }




            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(socios);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SociosExists(socios.Idsocio))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(socios);
        }

        public async Task<IActionResult> ListarSocios()
        {
            ViewBag.userId = HttpContext.Session.GetInt32("UserId");
            ViewBag.Nome = HttpContext.Session.GetString("Nome");
            return View(await _context.Socios.ToListAsync());
        }


        public IActionResult PerfilSocio(int? id)
        {
            int x = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));


            foreach (var item in _context.Socios)
            {
                if (item.Idsocio == x)
                {
                    var y = item;
                    return View(y);

                }
            }


            return View();
        }




        //public IActionResult  EfetuarPagamento()
        //{
        //    int x = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));


        //    foreach (var item in _context.Socios)
        //    {
        //        if (item.Idsocio == x)
        //        {
        //            var y = item;
        //            return View(y);

        //        }
        //    }
        //    return View();
        //}

       





        public IActionResult RegistarSocio()
        {
            return View();
        }





        // POST: Socios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegistarSocio([Bind("Idsocio,Email,Telefone,Fotografia,Sexo,Altura,NomeUtilizador,PesoInicial,Password,Estado")] Socios socios, IFormFile fotografia/*,Professores professores*//*,Administrador administrador*/)
        {
            ////verifica se o nome e o email ja existem
            //foreach (var item in _context.Socios)
            //{
            //    if ((administrador.Nome == item.NomeUtilizador) || (administrador.Email == item.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }



            //}

            //foreach (var item2 in _context.Administrador)
            //{
            //    if ((administrador.Nome == item2.Nome) || (administrador.Email == item2.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }


            //}


            //foreach (var item3 in _context.Professores)
            //{
            //    if ((administrador.Nome == item3.Nome) || (administrador.Email == item3.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }

            //}
            ////para os socios
            //foreach (var item4 in _context.Socios)
            //{
            //    if ((socios.NomeUtilizador == item4.NomeUtilizador) || (socios.Email == item4.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }



            //}

            //foreach (var item5 in _context.Administrador)
            //{
            //    if ((socios.NomeUtilizador == item5.Nome) || (socios.Email == item5.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }


            //}

            //foreach (var item6 in _context.Professores)
            //{
            //    if ((socios.NomeUtilizador == item6.Nome) || (socios.Email == item6.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }

            //}

            ////para os professores
            //foreach (var item7 in _context.Socios)
            //{
            //    if ((professores.Nome == item7.NomeUtilizador) || (professores.Email == item7.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }

            //}

            //foreach (var item8 in _context.Administrador)
            //{
            //    if ((professores.Nome == item8.Nome) || (professores.Email == item8.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }


            //}

            //foreach (var item9 in _context.Professores)
            //{
            //    if ((administrador.Nome == item9.Nome) || (administrador.Email == item9.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }

            //}
            string caminho = Path.Combine(_hostEnviroment.ContentRootPath, "wwwroot\\Fotos");

            string nome_ficheiro = Path.GetFileName(fotografia.FileName);
            string caminho_completo = Path.Combine(caminho, nome_ficheiro);

            FileStream fs = new FileStream(caminho_completo, FileMode.Create);
            fotografia.CopyTo(fs);
            socios.Fotografia = nome_ficheiro;


            fs.Close();


            if (ModelState.IsValid)
            {


                _context.Add(socios);
                await _context.SaveChangesAsync();
                return RedirectToAction("Login", "Home");
            }
            return View(socios);
        }



        // GET: Socios/Edit/5

        public async Task<IActionResult> EditarDadosSocio(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var socios = await _context.Socios.FindAsync(id);
            if (socios == null)
            {
                return NotFound();
            }
            return View(socios);


        }



        public async Task<IActionResult> Edit(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var socios = await _context.Socios.FindAsync(id);
            if (socios == null)
            {
                return NotFound();
            }
            return View(socios);
        }

        // POST: Socios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Edit(int id, [Bind("Idsocio,Email,Telefone,Fotografia,Sexo,Altura,NomeUtilizador,PesoInicial,Password,Estado")] Socios socios, IFormFile fotografia)
        {
            string caminho = Path.Combine(_hostEnviroment.ContentRootPath, "wwwroot\\Fotos");

            string nome_ficheiro = Path.GetFileName(fotografia.FileName);
            string caminho_completo = Path.Combine(caminho, nome_ficheiro);

            FileStream fs = new FileStream(caminho_completo, FileMode.Create);
            fotografia.CopyTo(fs);
            socios.Fotografia = nome_ficheiro;


            fs.Close();
            if (id != socios.Idsocio)
            {
                return NotFound();
            }




            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(socios);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SociosExists(socios.Idsocio))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(socios);
        }

        // GET: Socios/Delete/5

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var socios = await _context.Socios
                .FirstOrDefaultAsync(m => m.Idsocio == id);
            if (socios == null)
            {
                return NotFound();
            }

            return View(socios);
        }

        // POST: Socios/Delete/5

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var socios = await _context.Socios.FindAsync(id);
            _context.Socios.Remove(socios);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SociosExists(int id)
        {
            return _context.Socios.Any(e => e.Idsocio == id);
        }

        public static bool estaAutenticado(HttpContext contexto)
        {
            if (contexto.Session.GetInt32("UserId") != null)

                return true;
            else
                return false;
        }

       
    }
}
