﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using WebApplication1.filters;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
   
    public class AdministradoresController : Controller
    {
        private readonly GinasioContext _context;
        private readonly IHostEnvironment _hostEnviroment;
        public AdministradoresController(GinasioContext context, IHostEnvironment env)
        {
            _context = context;
            _hostEnviroment = env;
        }

        // GET: Administradores
        public async Task<IActionResult> Index()
        {
            ViewBag.userId = HttpContext.Session.GetInt32("AdminId");
            ViewBag.Nome = HttpContext.Session.GetString("Nome");
            return View(await _context.Administrador.ToListAsync());
        }

        // GET: Administradores/Details/5
        [myPerfilFilter(Perfil = "Administrador")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var administrador = await _context.Administrador
                .FirstOrDefaultAsync(m => m.Idadministrador == id);
            if (administrador == null)
            {
                return NotFound();
            }

            return View(administrador);
        }
        //[myPerfilFilter(Perfil = "Administrador")]
        public IActionResult Perfil()
        {
            int x = Convert.ToInt32(HttpContext.Session.GetInt32("AdminId"));


            foreach (var item in _context.Administrador)
            {
                if (item.Idadministrador == x)
                {
                    var y = item;
                    return View(y);

                }
            }


            return View();
            
        }

        public IActionResult RegistoPagamento()
        {
            return View();

        }



        // GET: Administradores/Create
        [myPerfilFilter(Perfil = "Administrador")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Administradores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [myPerfilFilter(Perfil = "Administrador")]
        public async Task<IActionResult> Create([Bind("Idadministrador,Nome,Email,Password")] Administrador administrador)
        {
            

            foreach (var item2 in _context.Administrador)
            {
                if (administrador.Nome == item2.Nome)
                {
                    return View();
                }
            }

            



            if (ModelState.IsValid)
            {
                _context.Add(administrador);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(administrador);
        }

        // GET: Administradores/Edit/5
        [myPerfilFilter(Perfil = "Administrador")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var administrador = await _context.Administrador.FindAsync(id);
            if (administrador == null)
            {
                return NotFound();
            }
            return View(administrador);
        }

        // POST: Administradores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [myPerfilFilter(Perfil = "Administrador")]
        public async Task<IActionResult> Edit(int id, [Bind("Idadministrador,Nome,Email,Password")] Administrador administrador)
        {
            if (id != administrador.Idadministrador)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(administrador);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AdministradorExists(administrador.Idadministrador))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(administrador);
        }

        // GET: Administradores/Delete/5
        [myPerfilFilter(Perfil = "Administrador")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var administrador = await _context.Administrador
                .FirstOrDefaultAsync(m => m.Idadministrador == id);
            if (administrador == null)
            {
                return NotFound();
            }

            return View(administrador);
        }

        // POST: Administradores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var administrador = await _context.Administrador.FindAsync(id);
            _context.Administrador.Remove(administrador);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AdministradorExists(int id)
        {
            return _context.Administrador.Any(e => e.Idadministrador == id);
        }
        public static bool estaAutenticado(HttpContext contexto)
        {
            if (contexto.Session.GetInt32("AdminId") != null)
                return true;
            else
                return false;
        }
    }
}