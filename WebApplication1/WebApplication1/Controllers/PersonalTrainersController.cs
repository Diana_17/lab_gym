﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class PersonalTrainersController : Controller
    {
        private readonly GinasioContext _context;

        public PersonalTrainersController(GinasioContext context)
        {
            _context = context;
        }

        // GET: PersonalTrainers
        public async Task<IActionResult> Index()
        {
            var ginasioContext = _context.PersonalTrainer.Include(p => p.IdprofessorNavigation).Include(p => p.IdsocioNavigation);
            return View(await ginasioContext.ToListAsync());
        }

        public async Task<IActionResult> VerPT( int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var personalTrainer = await _context.PersonalTrainer
                .Include(p => p.IdprofessorNavigation)
                .Include(p => p.IdsocioNavigation)
                .FirstOrDefaultAsync(m => m.Idsocio == id);
            if (personalTrainer == null)
            {
                return NotFound();
            }

            return View(personalTrainer);
        }



        // GET: PersonalTrainers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personalTrainer = await _context.PersonalTrainer
                .Include(p => p.IdprofessorNavigation)
                .Include(p => p.IdsocioNavigation)
                .FirstOrDefaultAsync(m => m.Idprofessor == id);
            if (personalTrainer == null)
            {
                return NotFound();
            }

            return View(personalTrainer);
        }
        public IActionResult VerificaPersonalTrainer(Socios socios, PersonalTrainer personalTrainer)
        {
            foreach (var item2 in _context.PersonalTrainer)
            {
                if ((socios.Idsocio == item2.Idsocio))
                {
                    ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email", personalTrainer.Idprofessor);
                    return View("PerfilProfessor");
                }
                else
                {
                    TempData["mensagemErro"] = "Utilizador não tem personal trainer!";
                }


            }

            return View();
        }


        //public IActionResult EscolherPT(PersonalTrainer personalTrainer, Socios socio, Professores professores,int ? id )
        //{
        //    if (id == professores.Idprofessor) { 

        //    personalTrainer.DataInicio = DateTime.Now;
        //    personalTrainer.Idsocio = socio.Idsocio;

        //        foreach (var item in _context.Socios)
        //        {
        //            if (item.NomeUtilizador == User.Identity.Name)
        //            {
        //                personalTrainer.Idsocio = item.Idsocio;
        //                personalTrainer.Idprofessor = professores.Idprofessor;

        //            }
        //        }
        //    }
        //    return View(personalTrainer);
        //}





        // GET: PersonalTrainers/Create
        public IActionResult Create()
        {
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email");
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email");
            return View();
        }


        // POST: PersonalTrainers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idprofessor,Idsocio,DataPedido,DataInicio,DataFim")] PersonalTrainer personalTrainer)
        {
            if (ModelState.IsValid)
            {
                _context.Add(personalTrainer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email", personalTrainer.Idprofessor);
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email", personalTrainer.Idsocio);
            return View(personalTrainer);
        }

        // GET: PersonalTrainers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personalTrainer = await _context.PersonalTrainer.FindAsync(id);
            if (personalTrainer == null)
            {
                return NotFound();
            }
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email", personalTrainer.Idprofessor);
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email", personalTrainer.Idsocio);
            return View(personalTrainer);
        }

        // POST: PersonalTrainers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idprofessor,Idsocio,DataPedido,DataInicio,DataFim")] PersonalTrainer personalTrainer)
        {
            if (id != personalTrainer.Idprofessor)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(personalTrainer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonalTrainerExists(personalTrainer.Idprofessor))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email", personalTrainer.Idprofessor);
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email", personalTrainer.Idsocio);
            return View(personalTrainer);
        }

        // GET: PersonalTrainers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personalTrainer = await _context.PersonalTrainer
                .Include(p => p.IdprofessorNavigation)
                .Include(p => p.IdsocioNavigation)
                .FirstOrDefaultAsync(m => m.Idprofessor == id);
            if (personalTrainer == null)
            {
                return NotFound();
            }

            return View(personalTrainer);
        }

        // POST: PersonalTrainers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var personalTrainer = await _context.PersonalTrainer.FindAsync(id);
            _context.PersonalTrainer.Remove(personalTrainer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PersonalTrainerExists(int id)
        {
            return _context.PersonalTrainer.Any(e => e.Idprofessor == id);
        }
    }
}
