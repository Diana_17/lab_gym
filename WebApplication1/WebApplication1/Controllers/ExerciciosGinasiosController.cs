﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ExerciciosGinasiosController : Controller
    {
        private readonly GinasioContext _context;
        private readonly IHostEnvironment _hostEnviroment;
        public ExerciciosGinasiosController(GinasioContext context, IHostEnvironment env)
        {
            _context = context;
            _hostEnviroment = env;
        }

        // GET: ExerciciosGinasios
        public async Task<IActionResult> Index()
        {
            return View(await _context.ExerciciosGinasio.ToListAsync());
        }
        public async Task<IActionResult> exercicios()
        {
            return View(await _context.ExerciciosGinasio.ToListAsync());
        }
        // GET: ExerciciosGinasios/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exerciciosGinasio = await _context.ExerciciosGinasio
                .FirstOrDefaultAsync(m => m.IdexerciciosGinasio == id);
            if (exerciciosGinasio == null)
            {
                return NotFound();
            }

            return View(exerciciosGinasio);
        }

        // GET: ExerciciosGinasios/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ExerciciosGinasios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdexerciciosGinasio,Nome,TextoDescritivo,Foto,Video")] ExerciciosGinasio exerciciosGinasio, IFormFile foto,IFormFile video)
        {
            

            if (ModelState.IsValid)
            {
                string caminho = Path.Combine(_hostEnviroment.ContentRootPath, "wwwroot\\Exercicios");

                string nome_ficheiro = Path.GetFileName(foto.FileName);
                string caminho_completo = Path.Combine(caminho, nome_ficheiro);

                FileStream fs = new FileStream(caminho_completo, FileMode.Create);
                foto.CopyTo(fs);
                exerciciosGinasio.Foto = caminho_completo;
                fs.Close();




                string caminho2 = Path.Combine(_hostEnviroment.ContentRootPath, "wwwroot\\Exercicios");

                string nome_ficheiro2 = Path.GetFileName(video.FileName);
                string caminho_completo2 = Path.Combine(caminho2, nome_ficheiro2);

                FileStream _fs = new FileStream(caminho_completo2, FileMode.Create);
                video.CopyTo(_fs);
                exerciciosGinasio.Video = caminho_completo2;
                _fs.Close();

                _context.Add(exerciciosGinasio);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(exerciciosGinasio);
        }

        // GET: ExerciciosGinasios/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exerciciosGinasio = await _context.ExerciciosGinasio.FindAsync(id);
            if (exerciciosGinasio == null)
            {
                return NotFound();
            }
            return View(exerciciosGinasio);
        }

        // POST: ExerciciosGinasios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdexerciciosGinasio,Nome,TextoDescritivo,Foto,Video")] ExerciciosGinasio exerciciosGinasio)
        {
            if (id != exerciciosGinasio.IdexerciciosGinasio)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(exerciciosGinasio);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExerciciosGinasioExists(exerciciosGinasio.IdexerciciosGinasio))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(exerciciosGinasio);
        }

        // GET: ExerciciosGinasios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exerciciosGinasio = await _context.ExerciciosGinasio
                .FirstOrDefaultAsync(m => m.IdexerciciosGinasio == id);
            if (exerciciosGinasio == null)
            {
                return NotFound();
            }

            return View(exerciciosGinasio);
        }

        // POST: ExerciciosGinasios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var exerciciosGinasio = await _context.ExerciciosGinasio.FindAsync(id);
            _context.ExerciciosGinasio.Remove(exerciciosGinasio);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExerciciosGinasioExists(int id)
        {
            return _context.ExerciciosGinasio.Any(e => e.IdexerciciosGinasio == id);
        }
    }
}
