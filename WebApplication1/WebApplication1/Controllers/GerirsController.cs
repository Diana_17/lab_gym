﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class GerirsController : Controller
    {
        private readonly GinasioContext _context;

        public GerirsController(GinasioContext context)
        {
            _context = context;
        }

        // GET: Gerirs
        public async Task<IActionResult> Index()
        {
            var ginasioContext = _context.Gerir.Include(g => g.IdadministradorNavigation).Include(g => g.IdsocioNavigation);
            return View(await ginasioContext.ToListAsync());
        }

        // GET: Gerirs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gerir = await _context.Gerir
                .Include(g => g.IdadministradorNavigation)
                .Include(g => g.IdsocioNavigation)
                .FirstOrDefaultAsync(m => m.Idsocio == id);
            if (gerir == null)
            {
                return NotFound();
            }

            return View(gerir);
        }

        // GET: Gerirs/Create
        public IActionResult Create()
        {
            ViewData["Idadministrador"] = new SelectList(_context.Administrador, "Idadministrador", "Email");
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email");
            return View();
        }

        // POST: Gerirs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DataSuspensao,MotivoSuspensao,Idsocio,Idadministrador")] Gerir gerir)
        {
            if (ModelState.IsValid)
            {
                _context.Add(gerir);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idadministrador"] = new SelectList(_context.Administrador, "Idadministrador", "Email", gerir.Idadministrador);
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email", gerir.Idsocio);
            return View(gerir);
        }

        // GET: Gerirs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gerir = await _context.Gerir.FindAsync(id);
            if (gerir == null)
            {
                return NotFound();
            }
            ViewData["Idadministrador"] = new SelectList(_context.Administrador, "Idadministrador", "Email", gerir.Idadministrador);
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email", gerir.Idsocio);
            return View(gerir);
        }

        // POST: Gerirs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DataSuspensao,MotivoSuspensao,Idsocio,Idadministrador")] Gerir gerir)
        {
            if (id != gerir.Idsocio)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(gerir);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GerirExists(gerir.Idsocio))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idadministrador"] = new SelectList(_context.Administrador, "Idadministrador", "Email", gerir.Idadministrador);
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email", gerir.Idsocio);
            return View(gerir);
        }

        // GET: Gerirs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gerir = await _context.Gerir
                .Include(g => g.IdadministradorNavigation)
                .Include(g => g.IdsocioNavigation)
                .FirstOrDefaultAsync(m => m.Idsocio == id);
            if (gerir == null)
            {
                return NotFound();
            }

            return View(gerir);
        }

        // POST: Gerirs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var gerir = await _context.Gerir.FindAsync(id);
            _context.Gerir.Remove(gerir);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GerirExists(int id)
        {
            return _context.Gerir.Any(e => e.Idsocio == id);
        }
    }
}
