﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class MensagemsController : Controller
    {
        private readonly GinasioContext _context;

        public MensagemsController(GinasioContext context)
        {
            _context = context;
        }

        // GET: Mensagems
        public async Task<IActionResult> Index()
        {
            var ginasioContext = _context.Mensagem.Include(m => m.IdprofessorNavigation).Include(m => m.IdsocioNavigation);
            return View(await ginasioContext.ToListAsync());
        }

        public async Task<IActionResult> VerMensagensSocio()
        {


            var ginasioContext = _context.Mensagem.Include(m => m.IdprofessorNavigation).Include(m => m.IdsocioNavigation);
            return View(await ginasioContext.ToListAsync());
        }

        public async Task<IActionResult> VerMensagensProf()
        {

            var ginasioContext = _context.Mensagem.Include(m => m.IdprofessorNavigation).Include(m => m.IdsocioNavigation);
            return View(await ginasioContext.ToListAsync());
        }

        // GET: Mensagems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mensagem = await _context.Mensagem
                .Include(m => m.IdprofessorNavigation)
                .Include(m => m.IdsocioNavigation)
                .FirstOrDefaultAsync(m => m.Idsocio == id);
            if (mensagem == null)
            {
                return NotFound();
            }

            return View();
        }

        // GET: Mensagems/Create
        public IActionResult Create()
        {
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email");
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email");
            return View();
        }

        // POST: Mensagems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DataEnvio,Texto,DataLeitura,Direcao,Idsocio,Idprofessor")] Mensagem mensagem)
        {
            if (ModelState.IsValid)
            {
                //false - para socio true- para prof 
                mensagem.DataEnvio = DateTime.Now;



                foreach (var item in _context.Socios)
                {
                    if (item.NomeUtilizador == User.Identity.Name)
                    {
                        mensagem.Idsocio = item.Idsocio;
                    }


                }
                foreach (var item in _context.Professores)
                {
                    if (item.Nome == User.Identity.Name)
                    {
                        mensagem.Idprofessor = item.Idprofessor;
                    }


                }


                _context.Add(mensagem);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email", mensagem.Idprofessor);
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email", mensagem.Idsocio);

            return RedirectToAction("Index", "Home");
        }

        // GET: Mensagems/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mensagem = await _context.Mensagem.FindAsync(id);
            if (mensagem == null)
            {
                return NotFound();
            }
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email", mensagem.Idprofessor);
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email", mensagem.Idsocio);
            return View(mensagem);
        }

        // POST: Mensagems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DataEnvio,Texto,DataLeitura,Direcao,Idsocio,Idprofessor")] Mensagem mensagem)
        {
            if (id != mensagem.Idsocio)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(mensagem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MensagemExists(mensagem.Idsocio))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email", mensagem.Idprofessor);
            ViewData["Idsocio"] = new SelectList(_context.Socios, "Idsocio", "Email", mensagem.Idsocio);
            return View(mensagem);
        }

        // GET: Mensagems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mensagem = await _context.Mensagem
                .Include(m => m.IdprofessorNavigation)
                .Include(m => m.IdsocioNavigation)
                .FirstOrDefaultAsync(m => m.Idsocio == id);
            if (mensagem == null)
            {
                return NotFound();
            }

            return View(mensagem);
        }

        // POST: Mensagems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var mensagem = await _context.Mensagem.FindAsync(id);
            _context.Mensagem.Remove(mensagem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MensagemExists(int id)
        {
            return _context.Mensagem.Any(e => e.Idsocio == id);
        }
    }
}
