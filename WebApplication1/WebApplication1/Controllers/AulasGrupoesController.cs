﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AulasGrupoesController : Controller
    {
        private readonly GinasioContext _context;
        private readonly IHostEnvironment _hostEnviroment;

        public AulasGrupoesController(GinasioContext context, IHostEnvironment env)
        {
            _context = context;
            _hostEnviroment = env;
        }

        // GET: AulasGrupoes
        public async Task<IActionResult> Index()
        {
            return View(await _context.AulasGrupo.ToListAsync());
        }
        public async Task<IActionResult> InscreverAula(int id)
        {
            return View(await _context.AulasGrupo.ToListAsync());
            
        }

        public async Task<IActionResult> AdminiAulas()
        {
            return View(await _context.AulasGrupo.ToListAsync());
        }

        public async Task<IActionResult> Inscrever(int id, Participa participa)
        {
            var Aula = from a in _context.AulasGrupo
                       select a;

            Aula = _context.AulasGrupo.Include(a => a.IdaulasGrupo).Include(a => a.MapaAulasGrupo);
            Aula = Aula.Where(a => a.IdaulasGrupo.Equals(id));
            //participa.IdmapaAulas = Aula;
            participa.Idsocio=id;
            

            return View(Aula.ToList());
        }


        // GET: AulasGrupoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aulasGrupo = await _context.AulasGrupo
                .FirstOrDefaultAsync(m => m.IdaulasGrupo == id);
            if (aulasGrupo == null)
            {
                return NotFound();
            }

            return View(aulasGrupo);
        }

        public async Task<IActionResult> DetalhesAula(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aulasGrupo = await _context.AulasGrupo
                .FirstOrDefaultAsync(m => m.IdaulasGrupo == id);
            if (aulasGrupo == null)
            {
                return NotFound();
            }

            return View(aulasGrupo);
        }

        // GET: AulasGrupoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AulasGrupoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(/*int id,*/ [Bind("IdaulasGrupo,Nome,Fotografia,TextoDescritivo,Estado")] AulasGrupo aulasGrupo,IFormFile fotografia)
        {
            string caminho = Path.Combine(_hostEnviroment.ContentRootPath, "wwwroot\\Aulas");
           

            string nome_ficheiro = Path.GetFileName(fotografia.FileName);
            string caminho_completo = Path.Combine(caminho, nome_ficheiro);

            FileStream fs = new FileStream(caminho_completo, FileMode.Create);
            fotografia.CopyTo(fs);
            aulasGrupo.Fotografia = caminho_completo;


            fs.Close();



            if (ModelState.IsValid)
            {
                //aulasGrupo.IdaulasGrupo = id;

                //foreach (var item in _context.Socios)
                //{
                //    if (item.NomeUtilizador == User.Identity.Name)
                //    {
                //        aulasGrupo.IdaulasGrupo = item.Idsocio;
                //    }
                //}

                _context.Add(aulasGrupo);
                await _context.SaveChangesAsync();
                //return RedirectToAction(nameof(Index));
                //return RedirectToAction("Details/"+ id,"AulasGrupoes");
                return RedirectToAction("AulasGrupoes");
            }
            return View(aulasGrupo);
        }

        // GET: AulasGrupoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aulasGrupo = await _context.AulasGrupo.FindAsync(id);
            if (aulasGrupo == null)
            {
                return NotFound();
            }
            return View(aulasGrupo);
        }

        // POST: AulasGrupoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdaulasGrupo,Nome,Fotografia,TextoDescritivo,Estado")] AulasGrupo aulasGrupo)
        {
            if (id != aulasGrupo.IdaulasGrupo)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(aulasGrupo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AulasGrupoExists(aulasGrupo.IdaulasGrupo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(aulasGrupo);
        }

        // GET: AulasGrupoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aulasGrupo = await _context.AulasGrupo
                .FirstOrDefaultAsync(m => m.IdaulasGrupo == id);
            if (aulasGrupo == null)
            {
                return NotFound();
            }

            return View(aulasGrupo);
        }

        // POST: AulasGrupoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var aulasGrupo = await _context.AulasGrupo.FindAsync(id);
            _context.AulasGrupo.Remove(aulasGrupo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AulasGrupoExists(int id)
        {
            return _context.AulasGrupo.Any(e => e.IdaulasGrupo == id);
        }
    }
}
