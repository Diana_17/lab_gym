﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class MapaAulasGrupoesController : Controller
    {
        private readonly GinasioContext _context;

        public MapaAulasGrupoesController(GinasioContext context)
        {
            _context = context;
        }

        // GET: MapaAulasGrupoes
        public async Task<IActionResult> Index()
        {
            var ginasioContext = _context.MapaAulasGrupo.Include(m => m.IdaulasGrupoNavigation).Include(m => m.IdprofessorNavigation);
            return View(await ginasioContext.ToListAsync());
        }

       

        // GET: MapaAulasGrupoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mapaAulasGrupo = await _context.MapaAulasGrupo
                .Include(m => m.IdaulasGrupoNavigation)
                .Include(m => m.IdprofessorNavigation)
                .FirstOrDefaultAsync(m => m.IdmapaAulas == id);
            if (mapaAulasGrupo == null)
            {
                return NotFound();
            }

            return View(mapaAulasGrupo);
        }

        // GET: MapaAulasGrupoes/Create
        public IActionResult Create()
        {
            ViewData["IdaulasGrupo"] = new SelectList(_context.AulasGrupo, "IdaulasGrupo", "Fotografia");
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email");
            return View();
        }

        // POST: MapaAulasGrupoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdmapaAulas,IdaulasGrupo,Idprofessor,Lotacao,DiaDaSemana,Hora")] MapaAulasGrupo mapaAulasGrupo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(mapaAulasGrupo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdaulasGrupo"] = new SelectList(_context.AulasGrupo, "IdaulasGrupo", "Fotografia", mapaAulasGrupo.IdaulasGrupo);
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email", mapaAulasGrupo.Idprofessor);
            return View(mapaAulasGrupo);
        }

        // GET: MapaAulasGrupoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mapaAulasGrupo = await _context.MapaAulasGrupo.FindAsync(id);
            if (mapaAulasGrupo == null)
            {
                return NotFound();
            }
            ViewData["IdaulasGrupo"] = new SelectList(_context.AulasGrupo, "IdaulasGrupo", "Fotografia", mapaAulasGrupo.IdaulasGrupo);
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email", mapaAulasGrupo.Idprofessor);
            return View(mapaAulasGrupo);
        }

        // POST: MapaAulasGrupoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdmapaAulas,IdaulasGrupo,Idprofessor,Lotacao,DiaDaSemana,Hora")] MapaAulasGrupo mapaAulasGrupo)
        {
            if (id != mapaAulasGrupo.IdmapaAulas)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(mapaAulasGrupo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MapaAulasGrupoExists(mapaAulasGrupo.IdmapaAulas))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdaulasGrupo"] = new SelectList(_context.AulasGrupo, "IdaulasGrupo", "Fotografia", mapaAulasGrupo.IdaulasGrupo);
            ViewData["Idprofessor"] = new SelectList(_context.Professores, "Idprofessor", "Email", mapaAulasGrupo.Idprofessor);
            return View(mapaAulasGrupo);
        }

        // GET: MapaAulasGrupoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mapaAulasGrupo = await _context.MapaAulasGrupo
                .Include(m => m.IdaulasGrupoNavigation)
                .Include(m => m.IdprofessorNavigation)
                .FirstOrDefaultAsync(m => m.IdmapaAulas == id);
            if (mapaAulasGrupo == null)
            {
                return NotFound();
            }

            return View(mapaAulasGrupo);
        }

        // POST: MapaAulasGrupoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var mapaAulasGrupo = await _context.MapaAulasGrupo.FindAsync(id);
            _context.MapaAulasGrupo.Remove(mapaAulasGrupo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MapaAulasGrupoExists(int id)
        {
            return _context.MapaAulasGrupo.Any(e => e.IdmapaAulas == id);
        }
    }
}
