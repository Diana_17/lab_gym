﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{

    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly GinasioContext _context;

        public HomeController(ILogger<HomeController> logger, GinasioContext _c)
        {
            _logger = logger;
            _context = _c;

        }
      
        public IActionResult Index()
        {
            
            return View();
        }
        
        public IActionResult Info()
        {
            return View();
        }

        

        public IActionResult AulasDeGrupo()
        {
            return View();
        }



        public IActionResult Login(string ReturnUrl)
        {
            return View();
        }


        [HttpPost]
        public IActionResult Login(string user, string password, string? ReturnUrl)
        {
            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));


            string passHash = Convert.ToBase64String(bytes);

            Socios socio = _context.Socios.Include(s => s.Gerir).Include(s => s.Mensagem).Include(s => s.Participa).Include(s => s.PersonalTrainer).Include(s => s.Peso).Include(s => s.PlanosExercicios).FirstOrDefault(s => s.NomeUtilizador == user && s.Password == password);
            if (socio != null)
            {
                HttpContext.Session.SetString("Nome", socio.NomeUtilizador);
                HttpContext.Session.SetInt32("UserId", socio.Idsocio);

                HttpContext.Session.SetString("Perfil", "Socio");

            }
            Professores prof = _context.Professores.Include(p => p.MapaAulasGrupo).Include(p => p.Mensagem).Include(p => p.Peso).Include(s => s.PersonalTrainer).Include(p => p.PlanosExercicios).FirstOrDefault(p => p.Nome == user && p.Password == password);
            if (prof != null)
            {
                HttpContext.Session.SetString("Nome", prof.Nome);
                HttpContext.Session.SetInt32("ProfId", prof.Idprofessor);


                HttpContext.Session.SetString("Perfil", "Professor");

            }
            Administrador admin = _context.Administrador.Include(a => a.Gerir).SingleOrDefault(a => a.Nome == user && a.Password == password);
            if (admin != null)
            {
                HttpContext.Session.SetString("Nome", admin.Nome);
                HttpContext.Session.SetInt32("AdminId", admin.Idadministrador);


                HttpContext.Session.SetString("Perfil", "Administrador");

            }

            if (String.IsNullOrEmpty(ReturnUrl))
            {
                //ModelState.AddModelError("","Utilizador inexistente na base de dados!");
                //TempData["mensagemErro"] = "Utilizador inexistente na base de dados!";
                //return View("NaoEncontrado","Socios");
                return LocalRedirect("/");
            }
            else
            {
                return LocalRedirect(ReturnUrl);
            }
        }




        public IActionResult EscolhaUser()
        {
            return View();
        }


       

        public IActionResult Logout()
        {
            HttpContext.Response.Cookies.Delete("CookieSessao");
            return LocalRedirect("/");
        }


        public IActionResult Registo()
        {
            return View();
        }



     
        [HttpPost]
        public IActionResult Registo(Administrador administrador,Socios socios,Professores professores)
        {

            //verifica se o nome e o email ja existem
            foreach (var item in _context.Socios)
            {
                if ((administrador.Nome == item.NomeUtilizador) &(administrador.Email == item.Email))
                {
                    ViewBag.MsgError = "Utilizador já está registado no sistema.";
                    //return View("Registo");
                }
                


            }

            foreach (var item2 in _context.Administrador)
            {
                if ((administrador.Nome == item2.Nome) & (administrador.Email == item2.Email))
                {
                    ViewBag.MsgError = "Utilizador já está registado no sistema.";
                    //return View("Registo");
                }
                

            }

            
            foreach (var item3 in _context.Professores)
            {
                if ((administrador.Nome == item3.Nome) &(administrador.Email == item3.Email))
                {
                    ViewBag.MsgError = "Utilizador já está registado no sistema.";
                    //return View("Registo");
                }
                
            }
            //para os socios
            foreach (var item4 in _context.Socios)
            {
                if ((socios.NomeUtilizador == item4.NomeUtilizador) & (socios.Email == item4.Email))
                {
                    ViewBag.MsgError = "Utilizador já está registado no sistema.";
                    //return View("Registo");
                }
               


            }

            foreach (var item5 in _context.Administrador)
            {
                if ((socios.NomeUtilizador == item5.Nome) & (socios.Email == item5.Email))
                {
                    ViewBag.MsgError = "Utilizador já está registado no sistema.";
                    //return View("Registo");
                }
                

            }

            foreach (var item6 in _context.Professores)
            {
                if ((socios.NomeUtilizador == item6.Nome) & (socios.Email == item6.Email))
                {
                    ViewBag.MsgError = "Utilizador já está registado no sistema.";
                    //return View("Registo");
                }
                
            }

            //para os professores
            foreach (var item7 in _context.Socios)
            {
                if ((professores.Nome == item7.NomeUtilizador) & (professores.Email == item7.Email))
                {
                    ViewBag.MsgError = "Utilizador já está registado no sistema.";
                    //return View("Registo");
                }
               
            }

            foreach (var item8 in _context.Administrador)
            {
                if ((professores.Nome == item8.Nome) & (professores.Email == item8.Email))
                {
                    ViewBag.MsgError = "Utilizador já está registado no sistema.";
                    //return View("Registo");
                }
                

            }

            foreach (var item9 in _context.Professores)
            {
                if ((administrador.Nome == item9.Nome) & (administrador.Email == item9.Email))
                {
                    ViewBag.MsgError = "Utilizador já está registado no sistema.";
                    //return View("Registo");
                }
                
            }

            return View("AceptRegisto"); 
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        public static bool estaAutenticado(HttpContext contexto)
        {
            if (contexto.Session.GetInt32("UserId") != null)
                return true;
            else
                return false;
        }







    }


}
