﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using WebApplication1.filters;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{

    public class ProfessoresController : Controller
    {
        private readonly GinasioContext _context;
        private readonly IHostEnvironment _hostEnviroment;

        public ProfessoresController(GinasioContext context, IHostEnvironment env)
        {
            _context = context;
            _hostEnviroment = env;
        }

        // GET: Professores
        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {

            ViewBag.userId = HttpContext.Session.GetInt32("ProfId");
            ViewBag.Nome = HttpContext.Session.GetString("Nome");
            return View(await _context.Professores.ToListAsync());
        }

        // GET: Professores/Details/5

        public async Task<IActionResult> Details(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            var professores = await _context.Professores
                .FirstOrDefaultAsync(m => m.Idprofessor == id);
            if (professores == null)
            {
                return NotFound();
            }

            return View();
        }

        public async Task<IActionResult> ListarProfessoresAdmin()
        {
            ViewBag.userId = HttpContext.Session.GetInt32("ProfId");
            ViewBag.Nome = HttpContext.Session.GetString("Nome");
            return View(await _context.Professores.ToListAsync());
        }

        public IActionResult PerfilProfessor()
        {
            int x = Convert.ToInt32(HttpContext.Session.GetInt32("ProfId"));


            foreach (var item in _context.Professores)
            {
                if (item.Idprofessor == x)
                {
                    var y = item;
                    return View(y);

                }
            }



            return View();
        }


        public async Task<IActionResult> VerProfessor(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            var professores = await _context.Professores
                .FirstOrDefaultAsync(m => m.Idprofessor == id);
            if (professores == null)
            {
                return NotFound();
            }

            return View(professores);



        }


        public async Task<IActionResult> EscolherPt(int? id)
        {


            if (id == null)
            {
                return NotFound();
            }
            var professores = await _context.Professores
                .FirstOrDefaultAsync(m => m.Idprofessor == id);
            
            if (professores == null)
            {
                return NotFound();
            }

            return View(professores);

        }

        //public IActionResult EscolherPt(PersonalTrainer personalTrainer, Socios socio, Professores professores)
        //{


        //        personalTrainer.DataInicio = DateTime.Now;
        //        personalTrainer.Idsocio = socio.Idsocio;

        //        foreach (var item in _context.Socios)
        //        {
        //            if (item.NomeUtilizador == User.Identity.Name)
        //            {
        //                personalTrainer.Idsocio = item.Idsocio;
        //                personalTrainer.Idprofessor = professores.Idprofessor;

        //            }
        //        }

        //    return View(personalTrainer);
        //}



        public async Task<IActionResult> EstadoEditar(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var professores = await _context.Professores.FindAsync(id);
            if (professores == null)
            {
                return NotFound();
            }
            return View(professores);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public async Task<IActionResult> EstadoEditar(int id, [Bind("Idprofessor,Nome,Email,Telefone,Fotografia,Sexo,Especialidade,Estado,Password")] Professores professores)
        {
            if (id != professores.Idprofessor)
            {
                return NotFound();
            }



            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(professores);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProfessoresExists(professores.Idprofessor))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(professores);
        }
        public async Task<IActionResult> ListarProfessores()
        {
            ViewBag.userId = HttpContext.Session.GetInt32("ProfId");
            ViewBag.Nome = HttpContext.Session.GetString("Nome");
            return View(await _context.Professores.ToListAsync());
        }

        [AllowAnonymous]

        // GET: Professores/Create
        public IActionResult RegistarProfessor()
        {
            return View();
        }

        // POST: Professores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegistarProfessor([Bind("Idprofessor,Nome,Email,Telefone,Fotografia,Sexo,Especialidade,Estado,Password")] Professores professores, IFormFile fotografia/*,Socios socios,Administrador administrador*/)
        {

            ////verifica se o nome e o email ja existem
            //foreach (var item in _context.Socios)
            //{
            //    if ((administrador.Nome == item.NomeUtilizador) || (administrador.Email == item.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }



            //}

            //foreach (var item2 in _context.Administrador)
            //{
            //    if ((administrador.Nome == item2.Nome) || (administrador.Email == item2.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }


            //}


            //foreach (var item3 in _context.Professores)
            //{
            //    if ((administrador.Nome == item3.Nome) || (administrador.Email == item3.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }

            //}
            ////para os socios
            //foreach (var item4 in _context.Socios)
            //{
            //    if ((socios.NomeUtilizador == item4.NomeUtilizador) || (socios.Email == item4.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }



            //}

            //foreach (var item5 in _context.Administrador)
            //{
            //    if ((socios.NomeUtilizador == item5.Nome) || (socios.Email == item5.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }


            //}

            //foreach (var item6 in _context.Professores)
            //{
            //    if ((socios.NomeUtilizador == item6.Nome) || (socios.Email == item6.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }

            //}

            ////para os professores
            //foreach (var item7 in _context.Socios)
            //{
            //    if ((professores.Nome == item7.NomeUtilizador) || (professores.Email == item7.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }

            //}

            //foreach (var item8 in _context.Administrador)
            //{
            //    if ((professores.Nome == item8.Nome) || (professores.Email == item8.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }


            //}

            //foreach (var item9 in _context.Professores)
            //{
            //    if ((administrador.Nome == item9.Nome) || (administrador.Email == item9.Email))
            //    {
            //        ViewBag.MsgError = "Utilizador já está registado no sistema.";
            //        //return View("Registo");
            //    }

            //}


            //vamos garantir o caminho correto para o ficheiro  para o guardar
            string caminho = Path.Combine(_hostEnviroment.ContentRootPath, "wwwroot\\Fotos");

            string nome_ficheiro = Path.GetFileName(fotografia.FileName);
            string caminho_completo = Path.Combine(caminho, nome_ficheiro);

            FileStream fs = new FileStream(caminho_completo, FileMode.Create);
            fotografia.CopyTo(fs);
            professores.Fotografia = nome_ficheiro;


            fs.Close();


            if (ModelState.IsValid)
            {




                _context.Add(professores);
                await _context.SaveChangesAsync();
                return RedirectToAction("Login", "Home");
            }
            return View(professores);
        }

        // GET: Professores/Edit/5
        [myPerfilFilter(Perfil = " Professor")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var professores = await _context.Professores.FindAsync(id);
            if (professores == null)
            {
                return NotFound();
            }
            return View(professores);
        }

        // POST: Professores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [myPerfilFilter(Perfil = " Professor")]
        public async Task<IActionResult> Edit(int id, [Bind("Idprofessor,Nome,Email,Telefone,Fotografia,Sexo,Especialidade,Estado,Password")] Professores professores)
        {
            if (id != professores.Idprofessor)
            {
                return NotFound();
            }



            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(professores);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProfessoresExists(professores.Idprofessor))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(professores);
        }

        // GET: Professores/Delete/5
        [myPerfilFilter(Perfil = " Professor")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var professores = await _context.Professores
                .FirstOrDefaultAsync(m => m.Idprofessor == id);
            if (professores == null)
            {
                return NotFound();
            }

            return View(professores);
        }

        // POST: Professores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var professores = await _context.Professores.FindAsync(id);
            _context.Professores.Remove(professores);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProfessoresExists(int id)
        {
            return _context.Professores.Any(e => e.Idprofessor == id);
        }

        public static bool estaAutenticado(HttpContext contexto)
        {
            if (contexto.Session.GetInt32("ProfId") != null)
                return true;
            else
                return false;
        }
    }
}
