﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApplication1.Models
{
    public partial class GinasioContext : DbContext
    {
        public GinasioContext()
        {
        }

        public GinasioContext(DbContextOptions<GinasioContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Administrador> Administrador { get; set; }
        public virtual DbSet<AulasGrupo> AulasGrupo { get; set; }
        public virtual DbSet<ExerciciosGinasio> ExerciciosGinasio { get; set; }
        public virtual DbSet<Gerir> Gerir { get; set; }
        public virtual DbSet<Inclui> Inclui { get; set; }
        public virtual DbSet<MapaAulasGrupo> MapaAulasGrupo { get; set; }
        public virtual DbSet<Mensagem> Mensagem { get; set; }
        public virtual DbSet<Participa> Participa { get; set; }
        public virtual DbSet<PersonalTrainer> PersonalTrainer { get; set; }
        public virtual DbSet<Peso> Peso { get; set; }
        public virtual DbSet<PlanosExercicios> PlanosExercicios { get; set; }
        public virtual DbSet<Professores> Professores { get; set; }
        public virtual DbSet<Socios> Socios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Ginasio;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Administrador>(entity =>
            {
                entity.HasKey(e => e.Idadministrador)
                    .HasName("PK__Administ__B1D14F091A3782D6");

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);
            });

            modelBuilder.Entity<AulasGrupo>(entity =>
            {
                entity.HasKey(e => e.IdaulasGrupo)
                    .HasName("PK__Aulas_Gr__98605FD23F3BA93F");

                entity.Property(e => e.Fotografia).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);

                entity.Property(e => e.TextoDescritivo).IsUnicode(false);
            });

            modelBuilder.Entity<ExerciciosGinasio>(entity =>
            {
                entity.HasKey(e => e.IdexerciciosGinasio)
                    .HasName("PK__Exercici__8CFA31663F63B859");

                entity.Property(e => e.Foto).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);

                entity.Property(e => e.TextoDescritivo).IsUnicode(false);

                entity.Property(e => e.Video).IsUnicode(false);
            });

            modelBuilder.Entity<Gerir>(entity =>
            {
                entity.HasKey(e => new { e.Idsocio, e.Idadministrador })
                    .HasName("PK__Gerir__8EF55BEC57939965");

                entity.Property(e => e.DataSuspensao).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MotivoSuspensao).IsUnicode(false);

                entity.HasOne(d => d.IdadministradorNavigation)
                    .WithMany(p => p.Gerir)
                    .HasForeignKey(d => d.Idadministrador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Gerir__IDAdminis__4F7CD00D");

                entity.HasOne(d => d.IdsocioNavigation)
                    .WithMany(p => p.Gerir)
                    .HasForeignKey(d => d.Idsocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Gerir__IDSocio__4E88ABD4");
            });

            modelBuilder.Entity<Inclui>(entity =>
            {
                entity.HasKey(e => new { e.IdexerciciosGinasio, e.IdPlanodeexercicios })
                    .HasName("PK__Inclui__5B1E074513A68E66");

                entity.HasOne(d => d.IdPlanodeexerciciosNavigation)
                    .WithMany(p => p.Inclui)
                    .HasForeignKey(d => d.IdPlanodeexercicios)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Inclui__IdPlanod__49C3F6B7");

                entity.HasOne(d => d.IdexerciciosGinasioNavigation)
                    .WithMany(p => p.Inclui)
                    .HasForeignKey(d => d.IdexerciciosGinasio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Inclui__IDExerci__4AB81AF0");
            });

            modelBuilder.Entity<MapaAulasGrupo>(entity =>
            {
                entity.HasKey(e => e.IdmapaAulas)
                    .HasName("PK__Mapa_Aul__4235D4C6653F3DE1");

                entity.Property(e => e.DiaDaSemana).IsUnicode(false);

                entity.HasOne(d => d.IdaulasGrupoNavigation)
                    .WithMany(p => p.MapaAulasGrupo)
                    .HasForeignKey(d => d.IdaulasGrupo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mapa_Aula__IDAul__3B75D760");

                entity.HasOne(d => d.IdprofessorNavigation)
                    .WithMany(p => p.MapaAulasGrupo)
                    .HasForeignKey(d => d.Idprofessor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mapa_Aula__IDPro__3A81B327");
            });

            modelBuilder.Entity<Mensagem>(entity =>
            {
                entity.HasKey(e => new { e.Idsocio, e.Idprofessor, e.DataEnvio })
                    .HasName("PK__Mensagem__76105D87751398BF");

                entity.Property(e => e.DataEnvio).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Texto).IsUnicode(false);

                entity.HasOne(d => d.IdprofessorNavigation)
                    .WithMany(p => p.Mensagem)
                    .HasForeignKey(d => d.Idprofessor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mensagem__IDProf__59063A47");

                entity.HasOne(d => d.IdsocioNavigation)
                    .WithMany(p => p.Mensagem)
                    .HasForeignKey(d => d.Idsocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mensagem__IDSoci__5812160E");
            });

            modelBuilder.Entity<Participa>(entity =>
            {
                entity.HasKey(e => new { e.Idsocio, e.Data, e.IdmapaAulas })
                    .HasName("PK__Particip__4D0D16292AFF2034");

                entity.Property(e => e.Data).HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.IdmapaAulasNavigation)
                    .WithMany(p => p.Participa)
                    .HasForeignKey(d => d.IdmapaAulas)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Participa__IDMap__403A8C7D");

                entity.HasOne(d => d.IdsocioNavigation)
                    .WithMany(p => p.Participa)
                    .HasForeignKey(d => d.Idsocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Participa__IDSoc__3F466844");
            });

            modelBuilder.Entity<PersonalTrainer>(entity =>
            {
                entity.HasKey(e => new { e.Idprofessor, e.Idsocio, e.DataPedido })
                    .HasName("PK__Personal__BE10AAD5C2D11811");

                entity.Property(e => e.DataPedido).HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.IdprofessorNavigation)
                    .WithMany(p => p.PersonalTrainer)
                    .HasForeignKey(d => d.Idprofessor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Personal___IDPro__33D4B598");

                entity.HasOne(d => d.IdsocioNavigation)
                    .WithMany(p => p.PersonalTrainer)
                    .HasForeignKey(d => d.Idsocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Personal___IDSoc__32E0915F");
            });

            modelBuilder.Entity<Peso>(entity =>
            {
                entity.HasKey(e => new { e.Idsocio, e.Idprofessor, e.DataPesagem })
                    .HasName("PK__Peso__491D34FE7F32B0CB");

                entity.Property(e => e.DataPesagem).HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.IdprofessorNavigation)
                    .WithMany(p => p.Peso)
                    .HasForeignKey(d => d.Idprofessor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Peso__IDProfesso__5441852A");

                entity.HasOne(d => d.IdsocioNavigation)
                    .WithMany(p => p.Peso)
                    .HasForeignKey(d => d.Idsocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Peso__IDSocio__534D60F1");
            });

            modelBuilder.Entity<PlanosExercicios>(entity =>
            {
                entity.HasKey(e => e.IdplanosExercicios)
                    .HasName("PK__Planos_E__C0C8EF5D1FD3319A");

                entity.Property(e => e.DataInicio).HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.IdprofessorNavigation)
                    .WithMany(p => p.PlanosExercicios)
                    .HasForeignKey(d => d.Idprofessor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Planos_Ex__IDPro__46E78A0C");

                entity.HasOne(d => d.IdsocioNavigation)
                    .WithMany(p => p.PlanosExercicios)
                    .HasForeignKey(d => d.Idsocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Planos_Ex__IDSoc__45F365D3");
            });

            modelBuilder.Entity<Professores>(entity =>
            {
                entity.HasKey(e => e.Idprofessor)
                    .HasName("PK__Professo__38B0B8AD9D8ECE87");

                entity.HasIndex(e => e.Email)
                    .HasName("UQ__Professo__AB6E616417C6F033")
                    .IsUnique();

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Especialidade).IsUnicode(false);

                entity.Property(e => e.Fotografia).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);
            });

            modelBuilder.Entity<Socios>(entity =>
            {
                entity.HasKey(e => e.Idsocio)
                    .HasName("PK__Socios__05E84F1CC4F44E18");

                entity.HasIndex(e => e.Email)
                    .HasName("UQ__Socios__AB6E61645D151F00")
                    .IsUnique();

                entity.HasIndex(e => e.NomeUtilizador)
                    .HasName("UQ__Socios__24D6AA7FBE361D5F")
                    .IsUnique();

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Fotografia).IsUnicode(false);

                entity.Property(e => e.NomeUtilizador).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Telefone).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
