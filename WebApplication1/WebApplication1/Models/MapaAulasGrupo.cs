﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    [Table("Mapa_Aulas_Grupo")]
    public partial class MapaAulasGrupo
    {
        public MapaAulasGrupo()
        {
            Participa = new HashSet<Participa>();
        }

        [Key]
        [Column("IDMapa_Aulas")]
        public int IdmapaAulas { get; set; }
        [Column("IDAulas_Grupo")]
        public int IdaulasGrupo { get; set; }
        [Column("IDProfessor")]
        public int Idprofessor { get; set; }
        [Column("lotacao")]
        public int Lotacao { get; set; }
        [Required]
        [Column("Dia_da_Semana")]
        [StringLength(30)]
        public string DiaDaSemana { get; set; }
        [Column("hora")]
        public TimeSpan Hora { get; set; }

        [ForeignKey(nameof(IdaulasGrupo))]
        [InverseProperty(nameof(AulasGrupo.MapaAulasGrupo))]
        public virtual AulasGrupo IdaulasGrupoNavigation { get; set; }
        [ForeignKey(nameof(Idprofessor))]
        [InverseProperty(nameof(Professores.MapaAulasGrupo))]
        public virtual Professores IdprofessorNavigation { get; set; }
        [InverseProperty("IdmapaAulasNavigation")]
        public virtual ICollection<Participa> Participa { get; set; }
    }
}
