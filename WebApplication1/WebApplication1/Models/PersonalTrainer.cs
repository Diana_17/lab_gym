﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    [Table("Personal_trainer")]
    public partial class PersonalTrainer
    {
        [Key]
        [Column("IDProfessor")]
        public int Idprofessor { get; set; }
        [Key]
        [Column("IDSocio")]
        public int Idsocio { get; set; }
        [Key]
        [Column("Data_Pedido", TypeName = "date")]
        public DateTime DataPedido { get; set; }
        [Column("data_Inicio", TypeName = "date")]
        public DateTime? DataInicio { get; set; }
        [Column("data_fim", TypeName = "date")]
        public DateTime? DataFim { get; set; }

        [ForeignKey(nameof(Idprofessor))]
        [InverseProperty(nameof(Professores.PersonalTrainer))]
        public virtual Professores IdprofessorNavigation { get; set; }
        [ForeignKey(nameof(Idsocio))]
        [InverseProperty(nameof(Socios.PersonalTrainer))]
        public virtual Socios IdsocioNavigation { get; set; }
    }
}
