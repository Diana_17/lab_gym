﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public partial class Administrador
    {
        public Administrador()
        {
            Gerir = new HashSet<Gerir>();
        }

        [Key]
        [Column("IDAdministrador")]
        public int Idadministrador { get; set; }
        [Required]
        [Column("nome")]
        [StringLength(50)]
        public string Nome { get; set; }
        [Required]
        [Column("email")]
        [StringLength(100)]
        public string Email { get; set; }
        [Required]
        [Column("password")]
        [StringLength(30)]
        public string Password { get; set; }

        [InverseProperty("IdadministradorNavigation")]
        public virtual ICollection<Gerir> Gerir { get; set; }
    }
}
