﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public partial class Gerir
    {
        [Column("data_suspensao", TypeName = "date")]
        public DateTime DataSuspensao { get; set; }
        [Required]
        [Column("motivo_suspensao")]
        [StringLength(100)]
        public string MotivoSuspensao { get; set; }
        [Key]
        [Column("IDSocio")]
        public int Idsocio { get; set; }
        [Key]
        [Column("IDAdministrador")]
        public int Idadministrador { get; set; }

        [ForeignKey(nameof(Idadministrador))]
        [InverseProperty(nameof(Administrador.Gerir))]
        public virtual Administrador IdadministradorNavigation { get; set; }
        [ForeignKey(nameof(Idsocio))]
        [InverseProperty(nameof(Socios.Gerir))]
        public virtual Socios IdsocioNavigation { get; set; }
    }
}
