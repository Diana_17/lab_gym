﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    [Table("Exercicios_Ginasio")]
    public partial class ExerciciosGinasio
    {
        public ExerciciosGinasio()
        {
            Inclui = new HashSet<Inclui>();
        }

        [Key]
        [Column("IDExercicios_Ginasio")]
        public int IdexerciciosGinasio { get; set; }
        [Required]
        [Column("nome")]
        [StringLength(30)]
        public string Nome { get; set; }
        [Required]
        [Column("texto_descritivo")]
        [StringLength(1000)]
        public string TextoDescritivo { get; set; }
        [Required]
        [Column("foto")]
        
        public string Foto { get; set; }
        [Required]
        [Column("video")]
        
        public string Video { get; set; }

        [InverseProperty("IdexerciciosGinasioNavigation")]
        public virtual ICollection<Inclui> Inclui { get; set; }
    }
}
