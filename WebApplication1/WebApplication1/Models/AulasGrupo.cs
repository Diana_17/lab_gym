﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    [Table("Aulas_Grupo")]
    public partial class AulasGrupo
    {
        public AulasGrupo()
        {
            MapaAulasGrupo = new HashSet<MapaAulasGrupo>();
        }

        [Key]
        [Column("IDAulas_Grupo")]
        public int IdaulasGrupo { get; set; }
        [Required]
        [Column("nome")]
        [StringLength(50)]
        public string Nome { get; set; }
        [Required]
        [Column("fotografia")]

        public string Fotografia { get; set; }
        [Required]
        [Column("texto_descritivo")]
        [StringLength(1000)]
        public string TextoDescritivo { get; set; }
        [Column("estado")]
        public bool Estado { get; set; }

        [InverseProperty("IdaulasGrupoNavigation")]
        public virtual ICollection<MapaAulasGrupo> MapaAulasGrupo { get; set; }
    }
}
