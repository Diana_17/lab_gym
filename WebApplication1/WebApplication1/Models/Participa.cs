﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public partial class Participa
    {
        [Key]
        [Column("_data", TypeName = "date")]
        public DateTime Data { get; set; }
        [Key]
        [Column("IDSocio")]
        public int Idsocio { get; set; }
        [Key]
        [Column("IDMapa_Aulas")]
        public int IdmapaAulas { get; set; }

        [ForeignKey(nameof(IdmapaAulas))]
        [InverseProperty(nameof(MapaAulasGrupo.Participa))]
        public virtual MapaAulasGrupo IdmapaAulasNavigation { get; set; }
        [ForeignKey(nameof(Idsocio))]
        [InverseProperty(nameof(Socios.Participa))]
        public virtual Socios IdsocioNavigation { get; set; }
    }
}
