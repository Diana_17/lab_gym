﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public partial class Mensagem
    {
        [Key]
        [Column("Data_Envio", TypeName = "date")]
        public DateTime DataEnvio { get; set; }
        [Required]
        [Column("texto")]
        [StringLength(1000)]
        public string Texto { get; set; }
        [Column("Data_leitura", TypeName = "date")]
        public DateTime? DataLeitura { get; set; }
        [Column("direcao")]
        public bool Direcao { get; set; } //false - para socio true- para prof 
        [Key]
        [Column("IDSocio")]
        public int Idsocio { get; set; }
        [Key]
        [Column("IDProfessor")]
        public int Idprofessor { get; set; }

        [ForeignKey(nameof(Idprofessor))]
        [InverseProperty(nameof(Professores.Mensagem))]
        public virtual Professores IdprofessorNavigation { get; set; }
        [ForeignKey(nameof(Idsocio))]
        [InverseProperty(nameof(Socios.Mensagem))]
        public virtual Socios IdsocioNavigation { get; set; }
    }
}
