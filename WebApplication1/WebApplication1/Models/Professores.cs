﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public partial class Professores
    {
        public Professores()
        {
            MapaAulasGrupo = new HashSet<MapaAulasGrupo>();
            Mensagem = new HashSet<Mensagem>();
            PersonalTrainer = new HashSet<PersonalTrainer>();
            Peso = new HashSet<Peso>();
            PlanosExercicios = new HashSet<PlanosExercicios>();
        }

        [Key]
        [Column("IDProfessor")]
        public int Idprofessor { get; set; }
        [Required]
        [Column("nome")]
        [StringLength(50)]
        public string Nome { get; set; }
        [Required]
        [Column("email")]
        [StringLength(100)]
        public string Email { get; set; }
        [Column("telefone")]
        public int Telefone { get; set; }
        [Required]
        [Column("fotografia")]
        
        public string Fotografia { get; set; }
        [Column("sexo")]
        public bool Sexo { get; set; }
        [Required]
        [Column("especialidade")]
        [StringLength(50)]
        public string Especialidade { get; set; }
        [Column("estado")]
        public int Estado { get; set; }
        [Required]
        [Column("_password")]
        [StringLength(20)]
        public string Password { get; set; }

        [InverseProperty("IdprofessorNavigation")]
        public virtual ICollection<MapaAulasGrupo> MapaAulasGrupo { get; set; }
        [InverseProperty("IdprofessorNavigation")]
        public virtual ICollection<Mensagem> Mensagem { get; set; }
        [InverseProperty("IdprofessorNavigation")]
        public virtual ICollection<PersonalTrainer> PersonalTrainer { get; set; }
        [InverseProperty("IdprofessorNavigation")]
        public virtual ICollection<Peso> Peso { get; set; }
        [InverseProperty("IdprofessorNavigation")]
        public virtual ICollection<PlanosExercicios> PlanosExercicios { get; set; }
    }
}
