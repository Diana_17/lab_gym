﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    [Table("Planos_Exercicios")]
    public partial class PlanosExercicios
    {
        public PlanosExercicios()
        {
            Inclui = new HashSet<Inclui>();
        }

        [Key]
        [Column("IDPlanos_Exercicios")]
        public int IdplanosExercicios { get; set; }
        [Column("IDSocio")]
        public int Idsocio { get; set; }
        [Column("IDProfessor")]
        public int Idprofessor { get; set; }
        [Column("data_inicio", TypeName = "date")]
        public DateTime DataInicio { get; set; }
        [Column("data_fim", TypeName = "date")]
        public DateTime? DataFim { get; set; }

        [ForeignKey(nameof(Idprofessor))]
        [InverseProperty(nameof(Professores.PlanosExercicios))]
        public virtual Professores IdprofessorNavigation { get; set; }
        [ForeignKey(nameof(Idsocio))]
        [InverseProperty(nameof(Socios.PlanosExercicios))]
        public virtual Socios IdsocioNavigation { get; set; }
        [InverseProperty("IdPlanodeexerciciosNavigation")]
        public virtual ICollection<Inclui> Inclui { get; set; }
    }
}
