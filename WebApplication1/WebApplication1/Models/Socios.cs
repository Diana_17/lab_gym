﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public partial class Socios
    {

        public Socios()
        {
            Gerir = new HashSet<Gerir>();
            Mensagem = new HashSet<Mensagem>();
            Participa = new HashSet<Participa>();
            PersonalTrainer = new HashSet<PersonalTrainer>();
            Peso = new HashSet<Peso>();
            PlanosExercicios = new HashSet<PlanosExercicios>();
        }

        [Key]
        [Column("IDSocio")]
        public int Idsocio { get; set; }
        [Required]
        [Column("email")]
        [StringLength(100)]
        public string Email { get; set; }
        [Required]
        [Column("telefone")]
        [StringLength(20)]
        public string Telefone { get; set; }
        [Required]
        [Column("fotografia")]
        [StringLength(40)]
        public string Fotografia { get; set; }
        [Column("sexo")]
        public bool Sexo { get; set; } // true- Feminino
                                       //  false" - Masculino
        [Column("altura")]
        public double Altura { get; set; }
        [Required]
        [Column("nome_utilizador")]
        [StringLength(50)]
        public string NomeUtilizador { get; set; }
        [Column("peso_inicial")]
        public double PesoInicial { get; set; }
        [Required]
        [Column("_password")]
        [StringLength(20)]
        public string Password { get; set; }
        [Column("estado")]
        public int Estado { get; set; } // 1 ativo, 0 suspenso 

        
        //[Column("mensalidade")]
        //public bool Mensalidade { get; set; } //0-nao pago  1-pago

        [InverseProperty("IdsocioNavigation")]
        public virtual ICollection<Gerir> Gerir { get; set; }
        [InverseProperty("IdsocioNavigation")]
        public virtual ICollection<Mensagem> Mensagem { get; set; }
        [InverseProperty("IdsocioNavigation")]
        public virtual ICollection<Participa> Participa { get; set; }
        [InverseProperty("IdsocioNavigation")]
        public virtual ICollection<PersonalTrainer> PersonalTrainer { get; set; }
        [InverseProperty("IdsocioNavigation")]
        public virtual ICollection<Peso> Peso { get; set; }
        [InverseProperty("IdsocioNavigation")]
        public virtual ICollection<PlanosExercicios> PlanosExercicios { get; set; }


    }
}
