﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public partial class Peso
    {
        [Key]
        [Column("Data_Pesagem", TypeName = "date")]
        public DateTime DataPesagem { get; set; }
        [Key]
        [Column("IDSocio")]
        public int Idsocio { get; set; }
        [Key]
        [Column("IDProfessor")]
        public int Idprofessor { get; set; }
        [Column("valor")]
        public double Valor { get; set; }

        [ForeignKey(nameof(Idprofessor))]
        [InverseProperty(nameof(Professores.Peso))]
        public virtual Professores IdprofessorNavigation { get; set; }
        [ForeignKey(nameof(Idsocio))]
        [InverseProperty(nameof(Socios.Peso))]
        public virtual Socios IdsocioNavigation { get; set; }
    }
}
