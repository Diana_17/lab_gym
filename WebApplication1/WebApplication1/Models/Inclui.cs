﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public partial class Inclui
    {
        [Column("duracao")]
        public TimeSpan? Duracao { get; set; }
        [Column("repeticoes")]
        public int? Repeticoes { get; set; }
        [Key]
        public int IdPlanodeexercicios { get; set; }
        [Key]
        [Column("IDExercicios_Ginasio")]
        public int IdexerciciosGinasio { get; set; }

        [ForeignKey(nameof(IdPlanodeexercicios))]
        [InverseProperty(nameof(PlanosExercicios.Inclui))]
        public virtual PlanosExercicios IdPlanodeexerciciosNavigation { get; set; }
        [ForeignKey(nameof(IdexerciciosGinasio))]
        [InverseProperty(nameof(ExerciciosGinasio.Inclui))]
        public virtual ExerciciosGinasio IdexerciciosGinasioNavigation { get; set; }
    }
}
