#pragma checksum "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "22e305a8cb56af4472b7aeb8676b9f6aeacfc6a5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Socios_Details), @"mvc.1.0.view", @"/Views/Socios/Details.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\_ViewImports.cshtml"
using WebApplication1;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\_ViewImports.cshtml"
using WebApplication1.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\_ViewImports.cshtml"
using WebApplication1.Controllers;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"22e305a8cb56af4472b7aeb8676b9f6aeacfc6a5", @"/Views/Socios/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8cd07336af8b51b4142997e7f9b3acc830b44dc0", @"/Views/_ViewImports.cshtml")]
    public class Views_Socios_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<WebApplication1.Models.Socios>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Fotos/ + model.Fotografia "), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
  
    ViewData["Title"] = "Details";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
    <style>
        body {
            padding-top: 0px;
            background-color: gray;
            background-image: url();
            background-image: url();
            background-repeat: no-repeat;
        }
    </style>

<div style=""color:white"">
    <h4>Detalhes Socios</h4>
    <hr />
    <dl class=""row"">
        <dt class = ""col-sm-2"">
            ");
#nullable restore
#line 22 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Email));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 25 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
       Write(Html.DisplayFor(model => model.Email));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 28 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Telefone));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 31 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
       Write(Html.DisplayFor(model => model.Telefone));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n");
            WriteLiteral("        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "22e305a8cb56af4472b7aeb8676b9f6aeacfc6a56827", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n");
#nullable restore
#line 41 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
             if (@Html.DisplayNameFor(model => model.Sexo) == "0")
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <p>Feminino</p>\r\n");
#nullable restore
#line 44 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
            }
            else
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <p>Masculino</p>\r\n");
#nullable restore
#line 48 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </dt>\r\n        <dd class = \"col-sm-10\">\r\n");
            WriteLiteral("        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 54 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Altura));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 57 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
       Write(Html.DisplayFor(model => model.Altura));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 60 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.NomeUtilizador));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 63 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
       Write(Html.DisplayFor(model => model.NomeUtilizador));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 66 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.PesoInicial));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 69 "C:\Users\Windows 10\Documents\CADEIRAS 3 ANO\LAB 4\etapa3\lab_gym\WebApplication1\WebApplication1\Views\Socios\Details.cshtml"
       Write(Html.DisplayFor(model => model.PesoInicial));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        \r\n");
            WriteLiteral("    </dl>\r\n</div>\r\n<div>\r\n    \r\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "22e305a8cb56af4472b7aeb8676b9f6aeacfc6a511249", async() => {
                WriteLiteral("Voltar <");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<WebApplication1.Models.Socios> Html { get; private set; }
    }
}
#pragma warning restore 1591
