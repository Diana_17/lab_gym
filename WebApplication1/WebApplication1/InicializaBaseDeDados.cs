﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1
{
    public class InicializaBaseDeDados
    {
        public static void Iniciar(GinasioContext context)
        {
            // verifica e garante que a BD existe
            context.Database.EnsureCreated();

            // analiza a(s) tabela(s) onde pretendemos garantir os dados
            if (context.Socios.Any() == false)
            {
                // prepara os dados para a tabela...
                SHA512 sha512 = SHA512Managed.Create();
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes("123456"));
                string pass1 = Convert.ToBase64String(bytes);

                bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes("654321"));
                string pass2 = Convert.ToBase64String(bytes);

                var s = new Socios[]
                {
                new Socios(){NomeUtilizador="António Serafim", Email="aserafim@aeiou.pt",Password=pass1 },
               
                };

                //... insere-os no model...
                foreach (var _Socio in s)
                {
                    context.Socios.Add(_Socio);
                }
                //...e atualiza a base de dados
                //context.SaveChanges();
            }

            if (context.Professores.Any() == false)
            {
                // prepara os dados para a tabela...
                SHA512 sha512 = SHA512Managed.Create();
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes("123456"));
                string pass1 = Convert.ToBase64String(bytes);

                bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes("654321"));
                string pass2 = Convert.ToBase64String(bytes);

                var p = new Professores[]
                {
                new Professores(){Nome="António Jose", Email="aJose@aeiou.pt",Password=pass1 },

                };

                //... insere-os no model...
                foreach (var _Prof in p)
                {
                    context.Professores.Add(_Prof);
                }
                //...e atualiza a base de dados
                //context.SaveChanges();
            }

        }
    }
}

